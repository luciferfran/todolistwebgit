package negocio;

import beans.Tarea;
import beans.Usuario;
import dao.TareaDao;

public class GestionTareas {

	
	TareaDao dao;
	
	public GestionTareas(){
		dao = new TareaDao();
	}
	
	public boolean guardarTarea(Tarea t){
		
		return dao.guardarTarea(t);
		
	}
	
	
	public boolean marcarTareaComoTerminada(int idTarea){
		Tarea tarea= dao.recuperarTarea(idTarea);
		tarea.setEstado(!tarea.isEstado());
		return dao.modificarTarea(tarea);
	}
	
	public boolean eliminarTarea(int id, Usuario user){
		return dao.eliminarTarea(id, user);
	}
	
	public int  eliminarTareasDeUsuario(Usuario user){
		return dao.eliminarTareasDeUsuario(user);
	}
	

	public void salir(){
		dao.cerrarConexion();
	}
	
	
}
