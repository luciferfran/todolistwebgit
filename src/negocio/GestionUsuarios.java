package negocio;

import beans.Usuario;
import dao.UsuarioDao;

public class GestionUsuarios {
	
	UsuarioDao dao;
	
	public GestionUsuarios(){
		dao = new UsuarioDao();
	}

	public boolean registrarUsuario(Usuario user) {
		return dao.registrarUsuario(user);
	}

	public Usuario logarUsuario(Usuario user) {
		Usuario userRecogido = dao.recuperarUsuario(user.getNombre());
		if(user!=null && user.getPass().equals(userRecogido.getPass() ))
			return userRecogido;
		else
		return null;
	}
	
	public Usuario recuperarUsuario(String nombre) {
		return dao.recuperarUsuario(nombre);
		}
	
	public void salir(){
		dao.cerrarConexion();
	}

}
