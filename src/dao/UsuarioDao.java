package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import beans.Usuario;

public class UsuarioDao {

	Connection con;
	TareaDao daoTarea;
	
	public UsuarioDao(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con= DriverManager.getConnection("jdbc:mysql://localhost/todolist2","root","root");
			daoTarea= new TareaDao();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*selecciona el driver a utilizar */ catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public boolean registrarUsuario(Usuario user) {
		try {
			
		String sql= "insert into usuarios values(?,?)";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, user.getNombre());
		ps.setString(2, user.getPass());
		ps.executeUpdate();
		return true;
		} catch (SQLException e) {

			e.printStackTrace();
			return false;
		}
		
	}


	public Usuario recuperarUsuario(String nombre) {
		String sql= "select * from usuarios where nombre=?";
	
		PreparedStatement ps;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1,nombre);
			ResultSet rs= ps.executeQuery();
			if(rs.next()){
				String pass= rs.getString("pass");
				Usuario user= new Usuario();
				user.setNombre(nombre);
				user.setPass(pass);
				user.setTareas(daoTarea.recuperarTareasDeUsuario(user));
				return user;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
		return null;
	}
	public void cerrarConexion(){
		try{
			con.close();
		}catch (Exception e) {
			System.out.println("No se puede cerrar la conexion");
			e.printStackTrace();
		}
	}
}
