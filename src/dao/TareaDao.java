package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import java.sql.PreparedStatement;

import java.util.ArrayList;
import beans.Tarea;
import beans.Usuario;

public class TareaDao {
	
	Connection con;
	
	
	public TareaDao(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con= DriverManager.getConnection("jdbc:mysql://localhost/todolist2","root","root");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} /*selecciona el driver a utilizar */ catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public boolean guardarTarea(Tarea t){
		try {
PreparedStatement sentencia = 
con.prepareStatement("insert into Tareas values(NULL,?, FALSE,?)");
sentencia.setString(1,t.getTitulo());
sentencia.setString(2,t.getUsuario().getNombre());
sentencia.executeUpdate();
/*	Statement sentencia= con.createStatement();
	sentencia.executeUpdate("insert into Tareas values(NULL,'"+t.getTitulo()+"', FALSE)");
	*/sentencia.close();	
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public List<Tarea> recuperarTareasDeUsuario(Usuario user){
		try {
			Statement sentencia= con.createStatement();
			ResultSet tablaTareas =sentencia.executeQuery("select * from Tareas where nombreUsuario=\""+user.getNombre()+"\"");
			ArrayList<Tarea> tareas= new ArrayList<Tarea>();
			while(tablaTareas.next()){
				int id= tablaTareas.getInt("id");
				String titulo = tablaTareas.getString("titulo");
				boolean estado = tablaTareas.getBoolean("estado");
				Tarea tarea = new Tarea(id,titulo,estado);
				tareas.add(tarea);
			}
				return tareas;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public Tarea recuperarTarea(int id){
		try {
			Statement sentencia= con.createStatement();
			ResultSet tablaTareas =sentencia.executeQuery("select * from Tareas where id="+id);
			tablaTareas.next();
			String titulo = tablaTareas.getString("titulo");
			boolean estado = tablaTareas.getBoolean("estado");
			Tarea tarea = new Tarea(id,titulo,estado);
			return tarea;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	public boolean modificarTarea(Tarea t){
		try {
			Statement sentencia= con.createStatement();
sentencia.executeUpdate("update Tareas SET estado="+t.isEstado()+" where id="+t.getId());
			sentencia.close();	
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	

	
	public boolean eliminarTarea(int  id, Usuario user){
		try {
			Statement sentencia= con.createStatement();
			int resultado =sentencia.executeUpdate("delete from Tareas where id="+id+" and nombreUsuario=\""+user.getNombre()+"\"");
			sentencia.close();
			if(resultado==0)
				return false;
			else
				return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public int eliminarTareasDeUsuario(Usuario user){
		try {
			Statement sentencia= con.createStatement();
			int resultado =sentencia.executeUpdate("delete from Tareas where nombreUsuario=\""+user.getNombre()+"\"");
			sentencia.close();	
			return resultado;
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		
	}
	
	public void cerrarConexion(){
		try{
			con.close();
		}catch (Exception e) {
			System.out.println("No se puede cerrar la conexion");
			e.printStackTrace();
		}
	}
	
}
