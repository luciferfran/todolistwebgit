DROP database IF EXISTS todolist2;
create database todolist2;
use todolist2;
create table usuarios (nombre VARCHAR(20) PRIMARY KEY, pass VARCHAR(16) NOT NULL);
CREATE TABLE tareas(id INT(5) PRIMARY KEY AUTO_INCREMENT, titulo VARCHAR(50) NOT NULL,estado BOOLEAN, nombreUsuario VARCHAR(20), CONSTRAINT FK_tareas_usuarios FOREIGN KEY (nombreUsuario) REFERENCES usuarios(nombre) ON DELETE cascade ON UPDATE cascade);
insert into usuarios values("victor","victor");
insert into tareas values(null, "Irme a casa", FALSE, "victor");
insert into tareas values(null, "Dar clase", TRUE, "victor");