package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import beans.Usuario;
import negocio.GestionUsuarios;

/**
 * Servlet implementation class Registrar
 */
@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GestionUsuarios gestion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        gestion= new GestionUsuarios();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre= request.getParameter("nombre");
		String pass = request.getParameter("pass");
		Usuario user = new Usuario();
		user.setNombre(nombre);
		user.setPass(pass);
		PrintWriter out= response.getWriter();
		out.println("<html><body><h3>");
		user= gestion.logarUsuario(user);
		if(user!=null){
			//Iniciar una session
			HttpSession sesion=request.getSession(true);
			sesion.setAttribute("usuario",user);
			response.sendRedirect("MostrarTareas");
		//	request.setAttribute("usuario", user.getNombre());
		//	RequestDispatcher rd=getServletContext().getRequestDispatcher("/MostrarTareas");
			//rd.forward(request,response); 
		
		}else{
			response.sendRedirect("error.html");
			//response.getWriter().println("Error de login");
		}
		out.println("</h3></body></html>");
	}

}
