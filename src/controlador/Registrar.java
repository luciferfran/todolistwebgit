package controlador;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Usuario;
import negocio.GestionUsuarios;

/**
 * Servlet implementation class Registrar
 */
@WebServlet("/registrar")
public class Registrar extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GestionUsuarios gestion;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Registrar() {
        super();
        gestion= new GestionUsuarios();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("registro.html");
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nombre= request.getParameter("nombre");
		String pass = request.getParameter("pass");
		Usuario user = new Usuario();
		user.setNombre(nombre);
		user.setPass(pass);
		//PrintWriter out= response.getWriter();
		//out.println("<html><body><h3>");
		if(gestion.registrarUsuario(user)){
			response.sendRedirect("registro.html");
			//response.getWriter().print("usuario registrado correctamente");
		}else{
			response.sendRedirect("error.html");
			//response.getWriter().println("No se ha podido registrar el usuario");
		}
	}

}
