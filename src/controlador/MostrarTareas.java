package controlador;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Tarea;
import beans.Usuario;
import negocio.GestionUsuarios;

/**
 * Servlet implementation class MostrarTareasVictor
 */
@WebServlet("/MostrarTareas")
public class MostrarTareas extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GestionUsuarios gestion;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public MostrarTareas() {
        super();
        gestion= new GestionUsuarios();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	String nombre=(String)request.getAttribute("usuario");
		HttpSession sesion=request.getSession(false);
		if(sesion == null){
			response.sendRedirect("loguin.html");
		}else{
			
		//String nombre= (String)sesion.getAttribute("nombreUsuario");
		//Usuario user = gestion.recuperarUsuario(nombre);
			Usuario user = (Usuario)sesion.getAttribute("usuario");
			PrintWriter out =response.getWriter();
		out.print("<html><body><table>");
		for (Tarea tarea : user.getTareas()) {
			String estado= tarea.isEstado()? "Realizado":"pendiente";
			out.println("<tr><td>"+tarea.getTitulo()+"</td><td>"+estado+"</td></tr>");
			}
		out.print("</table></body></html>");
	}
	}
		


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
