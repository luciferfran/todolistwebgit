package beans;

import java.util.ArrayList;
import java.util.List;

public class Usuario {

	
	private String nombre;
	private String pass;
	private List<Tarea> tareas;
	
	public Usuario(){
		tareas= new ArrayList<Tarea>();
	}
	

	public boolean add(Tarea arg0) {
		return tareas.add(arg0);
	}


	public Tarea get(int idTarea) {
		for (Tarea tarea : tareas) {
			if(tarea.getId()==idTarea)
				return tarea;
		}
		return null;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public List<Tarea> getTareas() {
		return tareas;
	}

	public void setTareas(List<Tarea> tareas) {
		this.tareas = tareas;
	}
	
}
