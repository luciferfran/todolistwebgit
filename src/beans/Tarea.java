package beans;

public class Tarea {
	
	private int id;
	private String titulo;
	private boolean estado;
	private Usuario usuario;
	

	public Tarea(){
		
	}
	public Tarea(int id, String titulo, boolean estado) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.estado = estado;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
